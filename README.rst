About Unicorn 
========

In the current Handbook, description of the Unicorn Online platform is provided. The Unicorn online platform consists of the main web-based
platform developed by Unicorn project consortium, and allows the design, deployment and management of secure and elastic-by design services that can be deployed in multi-cloud environments.