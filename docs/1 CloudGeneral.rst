.. _Cloud General:

****************************
Cloud Computing Fundamentals
****************************

*Selecting relevant resources in the field of Cloud Computing is on the one hand simple given the large amount of information available on the Internet, but at the same time can be a challenge if you really want to keep the best of the best. Below are the links to different very interesting resources with different levels of complexity so that the reader can adapt the reading to the knowledge they have of the different tools.*

* :ref:`GeneralArticulosD`
* :ref:`GeneralArticulosC`
* :ref:`GeneralVideos`
* :ref:`GeneralCourses`
* :ref:`GeneralBlogs`

