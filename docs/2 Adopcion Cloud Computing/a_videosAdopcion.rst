.. _GeneralVideosAdopcion:

Tutorials
######

.. topic:: Tutorials, Cloud Computing Adoption:

   **Title**: 47 advanced tutorials for mastering Kubernetes:

   **Source**: https://techbeacon.com/top-tutorials-mastering-kubernetes
                                              
   **Author**: Tech Beacon

   **Date**: 2017

   **Summary**: Best tutorials for mastering Kubernetes

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing Adoption:

   **Title**: Top Tutorials To Learn Kubernetes

   **Source**: https://medium.com/quick-code/top-tutorials-to-learn-kubernetes-e9507e76d9a4
                                              
   **Author**: Medium Blog

   **Date**: 2017

   **Summary**: Learn Kubernetes

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing Adoption:

   **Title**: Kubernetes cluster architecture:

   **Source**: https://www.coursera.org/lecture/deploy-micro-kube-ibm-cloud/kubernetes-cluster-architecture-9DY7Z
                                              
   **Author**: Coursera, IBM

   **Date**: 2017

   **Summary**:  First Kubernetes cluster architecture

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing Adoption:

   **Title**: Create first images in docker

   **Source**: https://docs.docker.com/develop/develop-images/baseimages/
                                              
   **Author**: Docker

   **Date**: 2016

   **Summary**: Create images in docker

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing Adoption:

   **Title**: Docker Series — Building your first image:

   **Source**: https://medium.com/pintail-labs/docker-series-building-your-first-image-8a6f051ae637      
                                        
   **Author**: Docker

   **Date**: 2016

   **Summary**: The Docker image we are going to build is the image used to start the pintail-whoami container we used in the article Docker Series — Starting your first container.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing Adoption:

   **Title**: Network Configuration

   **Source**: https://docs.docker.com/v1.5/articles/networking/   
                                        
   **Author**: Docker

   **Date**: 2016

   **Summary**: When Docker starts, it creates a virtual interface named docker0 on the host machine. It randomly chooses an address and subnet from the private range defined by RFC 1918 that are not in use on the host machine, and assigns it to docker0. Docker made the choice 172.17.42.1/16 when I started it a few minutes ago, for example — a 16-bit netmask providing 65,534 addresses for the host machine and its containers. The MAC address is generated using the IP address allocated to the container to avoid ARP collisions, using a range from 02:42:ac:11:00:00 to 02:42:ac:11:ff:ff...

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################