.. _GeneralArticulosDAdopcion:

Magazine articles
#################

.. topic:: Magazine articles, Cloud Computing Adoption:

   **Title**: What is Docker and why is it so darn popular?

   **Source**: https://www.zdnet.com/article/what-is-docker-and-why-is-it-so-darn-popular/
                                              
   **Author**: Steven J. Vaughan-Nichols

   **Date**: Nov 2016

   **Summary**: Docker is hotter than hot because it makes it possible to get far more apps running on the same old servers and it also makes it very easy to package and ship programs. Here's what you need to know about it.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Magazine articles, Cloud Computing Adoption:

   **Title**: A Beginner’s Guide to Kubernete:

   **Source**: https://medium.com/containermind/a-beginners-guide-to-kubernetes-7e8ca56420b6
                                              
   **Author**: Imesh Gunaratne

   **Date**: Aug 2017

   **Summary**: Kubernetes has now become the de facto standard for deploying containerized applications at scale in private, public and hybrid cloud environments. The largest public cloud platforms AWS, Google Cloud, Azure, IBM Cloud and Oracle Cloud now provide managed services for Kubernetes. A few years back RedHat completely replaced their OpenShift implementation with Kubernetes and collaborated with the Kubernetes community for implementing the next generation container platform. Mesosphere incorporated key features of Kubernetes such as container grouping, overlay networking, layer 4 routing, secrets, etc into their container platform DC/OS soon after Kubernetes got popular. DC/OS also integrated Kubernetes as a container orchestrator alongside Marathon. Pivotal recently introduced Pivotal Container Service (PKS) based on Kubernetes for deploying third-party services on Pivotal Cloud Foundry and as of today there are many other organizations and technology providers adapting it at a rapid phase.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Magazine articles, Cloud Computing Adoption:

   **Title**: What is Kubernetes? Container orchestration explained

   **Source**: https://www.infoworld.com/article/3268073/kubernetes/what-is-kubernetes-container-orchestration-explained.html
                                              
   **Author**: Serdar Yegulalp

   **Date**: Aug 2017

   **Summary**: The rise of containers has reshaped the way people think about developing, deploying, and maintaining software. Drawing on the native isolation capabilities of modern operating systems, containers support VM-like separation of concerns, but with far less overhead and far greater flexibility of deployment than hypervisor-based virtual machines.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################