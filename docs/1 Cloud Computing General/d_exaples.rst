.. _GeneralExamples:

Examples
########

.. topic:: Example, Cloud Computing:

   **Title**: Learn All About Microservices — Microservices Architecture Example:

   **Source**: https://dzone.com/articles/microservices-tutorial-learn-all-about-microservic   
                                           
   **Author**: DZone

   **Date**: May 2018

   **Summary**: This microservices tutorial is the third part in this microservices blog series. I hope that you have read my previous blog, What is Microservices, which explains the architecture, compares microservices with monoliths and SOA, and explores when to use microservices with the help of use cases.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################