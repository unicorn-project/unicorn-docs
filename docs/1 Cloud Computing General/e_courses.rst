.. _GeneralCourses:

Courses
#######

.. topic:: Courses, Cloud Computing:

   **Title**: Introduction cloud computing

   **Source**: https://www.edx.org/es/course/introduction-cloud-computing-microsoft-cloud200x
                 
   **Author**: Microsoft

   **Date**: 2018

   **Summary**: Cloud computing, or “the cloud”, has gone from a leading trend in IT to mainstream consciousness and wide adoption.

This self-paced course introduces cloud computing concepts where you’ll explore the basics of cloud services and cloud deployment models. 

You’ll become acquainted with commonly used industry terms, typical business scenarios and applications for the cloud, and benefits and limitations inherent in the new paradigm that is the cloud.

This course will help prepare you for more advanced courses in Windows Server-based cloud and datacenter administration.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################