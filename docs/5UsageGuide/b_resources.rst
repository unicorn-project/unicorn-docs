###############################################################
Managing Cloud Resources
###############################################################


========
Manage Cloud Resources
========
- Each user of UNICORN shall add cloud resources in compatible cloud providers in order to allow the deployment of an application
- OpenStack, Amazon AWS and Google Cloud are supported
- Appropriate forms for of the each cloud providers are available;

  + OpenStack
.. image:: assets/addresource_openstack.PNG

+ Amazon AWS
.. image:: assets/addresource_aws.PNG

+ Google Cloud
.. image:: assets/addresource_google.PNG

========
Manage Your Keys
========
You can add keys for your account with the appropriate form.

.. image:: assets/key_add.PNG


.. image:: assets/keylist.PNG
