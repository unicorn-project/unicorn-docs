###############################################################
Getting started with UCO Dashboard
###############################################################

Here you can find instructions for the basic functionalities of UCO Platform.

========
Register and Login
========


Login
------------



- Click the <Log in> button .

.. image:: assets/login.PNG

- Provide your login credentials and click the <SIGN IN> button.

.. image:: assets/login.PNG

- Upon successful authentication the following screen will be presented.

.. image:: assets/dash1.PNG


Logout
----------

- In order to perform logout  click the <Log-out > Button.

.. image:: assets/logout.png

- Upon successful logout the following screen will be presented.

.. image:: assets/login.PNG



Create account
--------


.. note::
  Registration is currently offered only by contacting us 

- Visit the Unicorn Platform `Unicorn Platform Webpage`_

.. _Unicorn Platform Webpage: https://uco.euprojects.net/

- When a user access the Unicorn the following screen is shown:

.. image:: assets/login.PNG

- Click on login button.

.. image:: assets/login.PNG

- Click on "Create Account" button.

.. image:: assets/login.PNG

- Provide account information and click on "CREATE ACCOUNT" button.

.. image:: assets/login.PNG

- Go to your email and click on Unicorn account information link.

.. image:: assets/login.PNG

- You have successfully Created an New Account.




========
Dashboard Main View
========

* Through the dashboard, an overview of the components, applications and deployed application instance is provided.
* In UNICORN, applications are described as microservices composed by smaller components.

* Also, an overview of the available and used, aggregated cloud resources of the user is provided.

.. image:: assets/dash1.PNG







==========
Platform Usage Video
==========

This screencast can also help you get started:

.. raw:: html

    <div style="text-align: center; margin-bottom: 2em;">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/faCqx9EGRAo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

